/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.device;

import com.orionasa.factory.protocol.FMProtocol;

/**
 *
 * @author orion
 */
public class Device {

    private String uuid;
    private String name;
    private String category;
    private String status;
    private int unit;
    private String activeTask;

    public Device() {
        this.status = FMProtocol.STATUS_EMPTY;
    }
    
    public Device(String name, String category, String status) {
        this.name = name;
        this.category = category;
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActiveTask() {
        return activeTask;
    }

    public void setActiveTask(String activeTask) {
        this.activeTask = activeTask;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }
    
    
    
    
    
}
