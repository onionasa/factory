package com.orionasa.factory.device;


import com.orionasa.factory.protocol.FMProtocol;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.boon.Boon;
import org.codehaus.jackson.map.ObjectMapper;

public class TCPDeviceClient {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Device device = new Device();
        System.out.println("Enter device name ");
        device.setName(scanner.nextLine());
        System.out.println("Enter device category ");
        device.setCategory(scanner.nextLine());
        System.out.println("Enter device unit per/sec ");
        device.setUnit(Integer.parseInt(scanner.nextLine()));
        System.out.println("Device created " + device.getName());
        String serverHostname = "127.0.0.1";
        int port = 4141;
        System.out.println("Attemping to connect to host "
                + serverHostname + " on port 4141");

        Socket echoSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            echoSocket = new Socket(serverHostname, 4141);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                    echoSocket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for "
                    + "the connection to: " + serverHostname);
            System.exit(1);
        }
        //{"from":"device","method":"deviceCreate","name":"Tezgah 1","category":"DOKUM","status":"empty","deviceUnit":"unit"}
        Map<String, String> deviceCreate = new HashMap<>();
        deviceCreate.put(FMProtocol.FROM, FMProtocol.DEVICE);
        deviceCreate.put(FMProtocol.METHOD, FMProtocol.DEVICE_CREATE);
        deviceCreate.put(FMProtocol.DEVICE_NAME, device.getName());
        deviceCreate.put(FMProtocol.DEVICE_CATEGORY, device.getCategory());
        deviceCreate.put(FMProtocol.STATUS, device.getStatus());
        deviceCreate.put(FMProtocol.DEVICE_UNIT, String.valueOf(device.getUnit()));
        out.println(Boon.toJson(deviceCreate));
        String data;
        Map serializedData;
        ObjectMapper objectMapper = new ObjectMapper();

        while ((data = in.readLine()) != null) {
            serializedData = objectMapper.readValue(data, Map.class);
            if (serializedData.keySet().contains(FMProtocol.METHOD)) {
                Map<String, String> response = new HashMap<>();
                switch (serializedData.get(FMProtocol.METHOD).toString()) {
                    case FMProtocol.DEVICE_CREATE_RESPONSE:
                        // {"from":"server","method":"deviceCreateResponse","status":"ok","deviceUuid":"device"}
                        device.setUuid(serializedData.get(FMProtocol.DEVICE_UUID).toString());
                        break;
                    case FMProtocol.DEVICE_SET_TASK:
                        // {"from":"server","method":"deviceSetTask","taskUnit":"3","taskId":"task"}
                        device.setActiveTask(serializedData.get(FMProtocol.TASK_ID).toString());
                        DeviceManager deviceManager = new DeviceManager(echoSocket,
                                device,
                                Integer.parseInt(serializedData.get(FMProtocol.TASK_UNIT).toString()));
                        deviceManager.startDeviceManager();
                        break;
                    default:
                        System.out.println("Server message " + data);
                        break;
                }
            }

        }

        out.close();
        in.close();
        echoSocket.close();
    }
}
