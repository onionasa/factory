/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.device;

import com.orionasa.factory.protocol.FMProtocol;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.boon.Boon;

/**
 *
 * @author orion
 */
public class DeviceManager extends Thread {

    private final Socket sc;
    private final Device device;
    private final int taskUnit;

    public DeviceManager(Socket sc, Device device, int taskUnit) {
        this.sc = sc;
        this.device = device;
        this.taskUnit = taskUnit;
    }

    public void startDeviceManager() {
        start();
    }

    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(sc.getOutputStream(),
                    true);
            Map<String, String> response = new HashMap<>();
            device.setStatus(FMProtocol.STATUS_BUSY);
            response.put(FMProtocol.FROM, FMProtocol.DEVICE);
            response.put(FMProtocol.METHOD, FMProtocol.DEVICE_STATUS_CHANGED);
            response.put(FMProtocol.STATUS, device.getStatus());
            out.println(Boon.toJson(response));
            Thread.sleep(1000 * (taskUnit / device.getUnit()));
            device.setStatus(FMProtocol.STATUS_EMPTY);
            response.clear();
            response.put(FMProtocol.FROM, FMProtocol.DEVICE);
            response.put(FMProtocol.METHOD, FMProtocol.DEVICE_STATUS_CHANGED);
            response.put(FMProtocol.STATUS, device.getStatus());
            response.put(FMProtocol.TASK_ID, device.getActiveTask());
            out.println(Boon.toJson(response));
        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(DeviceManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
