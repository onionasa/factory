/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.user;

import com.orionasa.factory.server.Task;
import com.orionasa.factory.protocol.FMProtocol;
import java.awt.Color;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import org.boon.Boon;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author orion
 */
public class DeviceDetail extends javax.swing.JFrame {

    private final String uuid;
    private final Socket echoSocket;
    private final String device;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * Creates new form UserMainFrame
     */
    /**
     * Creates new form UserMainFrame
     *
     * @param echoSocket
     * @param uuid
     * @param username
     */
    public DeviceDetail(Socket echoSocket, String uuid, String username, String device) {
        initComponents();
        this.username.setText(username);
        this.uuid = uuid;
        this.echoSocket = echoSocket;
        this.device = device;
        Thread t1 = new Thread(() -> {
            fetchData();
        });
        t1.start();

    }

    private void fetchData() {
        taskTable.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Id", "Category", "Device", "Unit", "Status"
                }
        ) {
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        });

        DefaultTableModel model = (DefaultTableModel) taskTable.getModel();
        if (echoSocket != null) {
            try {
                out = new PrintWriter(echoSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(
                        echoSocket.getInputStream()));
                Map<String, Object> request = new HashMap<>();
                request.put(FMProtocol.FROM, FMProtocol.USER);
                request.put(FMProtocol.METHOD, FMProtocol.DEVICE_INFO);
                request.put(FMProtocol.USER_UUID, this.uuid);
                request.put(FMProtocol.DEVICE_UUID, this.device);
                out.println(Boon.toJson(request));
                String data;
                Map serializedData;
                Boolean isDone = false;
                ObjectMapper objectMapper = new ObjectMapper();
                try {
                    while ((data = in.readLine()) != null && !isDone) {
                        serializedData = objectMapper.readValue(data, Map.class);
                        if (serializedData.keySet().contains(FMProtocol.METHOD)) {
                            Map<String, String> response = new HashMap<>();
                            switch (serializedData.get(FMProtocol.METHOD).toString()) {
                                case FMProtocol.DEVICE_INFO_RESPONSE:
                                    // {"from":"server","method":"serverStatus","status":"online"}
                                    if (!serializedData.get(FMProtocol.STATUS).equals(FMProtocol.ERROR)) {
                                        Map<String, Task> taskList = (Map<String, Task>) serializedData.get(FMProtocol.TASK);
                                        Iterator iterator = taskList.values().iterator();
                                        idForm.setText(serializedData.get(FMProtocol.DEVICE_UUID).toString());
                                        nameForm.setText(serializedData.get(FMProtocol.DEVICE_NAME).toString());
                                        categoryForm.setText(serializedData.get(FMProtocol.DEVICE_CATEGORY).toString());
                                        statusForm.setText(serializedData.get(FMProtocol.STATUS).toString());
                                        unitForm.setText(serializedData.get(FMProtocol.DEVICE_UNIT).toString());
                                        while (iterator.hasNext()) {
                                            LinkedHashMap task = (LinkedHashMap) iterator.next();
                                            if (!task.keySet().contains("uuid")) {
                                                model.addRow(new Object[]{task.get("id"), task.get("name"), "", task.get("unit"), task.get("status")});
                                            } else {
                                                model.addRow(new Object[]{task.get("id"), task.get("name"), task.get("uuid"), task.get("unit"), task.get("status")});
                                            }
                                        }
                                        isDone = true;
                                    } else {
                                        System.out.println(serializedData);
                                        JOptionPane.showMessageDialog(this, "Device not found", "Error", JOptionPane.ERROR_MESSAGE);
                                        isDone = true;
                                    }
                                    break;
                                case FMProtocol.USER_NOT_SECURE:
                                    // {"from":"server","method":"userNotSecure"}
                                    JOptionPane.showMessageDialog(this, "User not authenticated", "Error", JOptionPane.ERROR_MESSAGE);
                                    isDone = true;
                                    break;
                                default:
                                    System.out.println("Server messages " + data);
                                    isDone = true;
                                    break;
                            }
                        }
                        if (isDone) {
                            break;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Server offline", "Info", JOptionPane.WARNING_MESSAGE);
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        username = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taskTable = new javax.swing.JTable(){
            @Override
            public Component prepareRenderer(TableCellRenderer renderer, int row, int col) {
                Component comp = super.prepareRenderer(renderer, row, col);
                Object value = getModel().getValueAt(row, col);
                if (value.equals(FMProtocol.STATUS_QUEUED)) {
                    comp.setBackground(Color.MAGENTA);
                } else if (value.equals(FMProtocol.STATUS_PROCESSING)) {
                    comp.setBackground(Color.YELLOW);
                } else if (value.equals(FMProtocol.STATUS_DONE)){
                    comp.setBackground(Color.GREEN);
                }else{
                    comp.setBackground(Color.LIGHT_GRAY);
                }
                return comp;
            }
        };
        jLabel3 = new javax.swing.JLabel();
        idForm = new javax.swing.JLabel();
        categoryForm = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nameForm = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        statusForm = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        refresh = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        unitForm = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Factory Management System - Device Detail");

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("Welcome ");

        username.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        username.setText("-");

        taskTable.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
        taskTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Category", "Device", "Unit", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }

        });
        jScrollPane1.setViewportView(taskTable);

        jLabel3.setText("Device Id:");

        idForm.setText("-");

        categoryForm.setText("-");

        jLabel6.setText("Device Category:");

        nameForm.setText("-");

        jLabel8.setText("Device Name:");

        statusForm.setText("-");

        jLabel10.setText("Device Status:");

        refresh.setText("Refresh");
        refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel4.setText("Device Task List");

        jLabel11.setText("Device Unit:");

        unitForm.setText("-");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(168, 168, 168)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(categoryForm, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(idForm, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(nameForm, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(187, 187, 187))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(statusForm, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53)
                        .addComponent(jLabel11)
                        .addGap(18, 18, 18)
                        .addComponent(unitForm, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(refresh)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(username))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(idForm)
                    .addComponent(jLabel6)
                    .addComponent(categoryForm)
                    .addComponent(jLabel8)
                    .addComponent(nameForm))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(statusForm)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(unitForm)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(refresh)
                .addGap(12, 12, 12))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            fetchData();
        });
        t1.start();
    }//GEN-LAST:event_refreshActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel categoryForm;
    private javax.swing.JLabel idForm;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nameForm;
    private javax.swing.JButton refresh;
    private javax.swing.JLabel statusForm;
    private javax.swing.JTable taskTable;
    private javax.swing.JLabel unitForm;
    private javax.swing.JLabel username;
    // End of variables declaration//GEN-END:variables
}
