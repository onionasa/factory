/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.user;

import com.orionasa.factory.protocol.FMProtocol;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.boon.Boon;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author orion
 */
public class UserMainFrame extends javax.swing.JFrame {

    private final String uuid;
    private final Socket echoSocket;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * Creates new form UserMainFrame
     */
    /**
     * Creates new form UserMainFrame
     *
     * @param echoSocket
     * @param uuid
     * @param username
     */
    public UserMainFrame(Socket echoSocket, String uuid, String username) {
        initComponents();
        this.username.setText(username);
        this.uuid = uuid;
        this.echoSocket = echoSocket;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        deviceList = new javax.swing.JButton();
        taskList = new javax.swing.JButton();
        createTask = new javax.swing.JButton();
        createUser = new javax.swing.JButton();
        serverStatus = new javax.swing.JButton();
        serverStatus1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        username = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel1.setText("Factory Management System");

        deviceList.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        deviceList.setText("Device List");
        deviceList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deviceListActionPerformed(evt);
            }
        });

        taskList.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        taskList.setText("Task List");
        taskList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                taskListActionPerformed(evt);
            }
        });

        createTask.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        createTask.setText("Create Task");
        createTask.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createTaskActionPerformed(evt);
            }
        });

        createUser.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        createUser.setText("Create User");
        createUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createUserActionPerformed(evt);
            }
        });

        serverStatus.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        serverStatus.setText("Server Status");
        serverStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serverStatusActionPerformed(evt);
            }
        });

        serverStatus1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        serverStatus1.setText("Disconnect");
        serverStatus1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serverStatus1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel2.setText("Welcome ");

        username.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        username.setText("-");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(87, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(deviceList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(taskList, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(createTask, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(createUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(serverStatus, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(serverStatus1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(85, 85, 85))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(username))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(deviceList)
                .addGap(18, 18, 18)
                .addComponent(taskList)
                .addGap(18, 18, 18)
                .addComponent(createTask)
                .addGap(18, 18, 18)
                .addComponent(createUser)
                .addGap(18, 18, 18)
                .addComponent(serverStatus)
                .addGap(18, 18, 18)
                .addComponent(serverStatus1)
                .addGap(28, 28, 28))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void serverStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serverStatusActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            if (echoSocket != null) {
                try {
                    out = new PrintWriter(echoSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(
                            echoSocket.getInputStream()));
                    Map<String, Object> request = new HashMap<>();
                    request.put(FMProtocol.FROM, FMProtocol.USER);
                    request.put(FMProtocol.METHOD, FMProtocol.SERVER_STATUS);
                    request.put(FMProtocol.USER_UUID, this.uuid);
                    out.println(Boon.toJson(request));
                    String data;
                    Map serializedData;
                    Boolean isDone = false;
                    ObjectMapper objectMapper = new ObjectMapper();
                    try {
                        while ((data = in.readLine()) != null && !isDone) {
                            serializedData = objectMapper.readValue(data, Map.class);
                            if (serializedData.keySet().contains(FMProtocol.METHOD)) {
                                Map<String, String> response = new HashMap<>();
                                switch (serializedData.get(FMProtocol.METHOD).toString()) {
                                    case FMProtocol.SERVER_STATUS:
                                        // {"from":"server","method":"serverStatus","status":"online"}
                                        JOptionPane.showMessageDialog(this, "Server status - " + serializedData.get(FMProtocol.STATUS), "Info", JOptionPane.INFORMATION_MESSAGE);
                                        isDone = true;
                                        break;
                                    case FMProtocol.USER_NOT_SECURE:
                                        // {"from":"server","method":"userNotSecure"}
                                        JOptionPane.showMessageDialog(this, "User not authenticated", "Error", JOptionPane.ERROR_MESSAGE);
                                        isDone = true;
                                        break;
                                    default:
                                        System.out.println("Server messagef " + data);
                                        break;
                                }
                            }
                            if (isDone) {
                                break;
                            }
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(UserLogin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(this, "Server offline", "Info", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        t1.start();
    }//GEN-LAST:event_serverStatusActionPerformed

    private void serverStatus1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serverStatus1ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_serverStatus1ActionPerformed

    private void createUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createUserActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            CreateUser crtUser = new CreateUser(echoSocket, uuid, username.getText());
            crtUser.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
            crtUser.setVisible(true);
        });
        t1.start();
    }//GEN-LAST:event_createUserActionPerformed

    private void createTaskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createTaskActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            CreateTask crtTask = new CreateTask(echoSocket, uuid, username.getText());
            crtTask.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
            crtTask.setVisible(true);
        });
        t1.start();
    }//GEN-LAST:event_createTaskActionPerformed

    private void taskListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_taskListActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            TaskList lstTask = new TaskList(echoSocket, uuid, username.getText());
            lstTask.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
            lstTask.setVisible(true);
        });
        t1.start();
    }//GEN-LAST:event_taskListActionPerformed

    private void deviceListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deviceListActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(() -> {
            DeviceList lstDevice = new DeviceList(echoSocket, uuid, username.getText());
            lstDevice.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
            lstDevice.setVisible(true);
        });
        t1.start();
    }//GEN-LAST:event_deviceListActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createTask;
    private javax.swing.JButton createUser;
    private javax.swing.JButton deviceList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton serverStatus;
    private javax.swing.JButton serverStatus1;
    private javax.swing.JButton taskList;
    private javax.swing.JLabel username;
    // End of variables declaration//GEN-END:variables
}
