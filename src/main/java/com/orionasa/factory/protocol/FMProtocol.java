/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.protocol;

/**
 *
 * @author orion
 */
public class FMProtocol {
    public final static String STATUS = "status";
    public final static String STATUS_EMPTY = "empty";
    public final static String STATUS_BUSY = "busy";
    public final static String STATUS_DISCONNECTED = "disconnected";
    public final static String STATUS_ONLINE = "online";
    public final static String STATUS_QUEUED = "queued";
    public final static String STATUS_PROCESSING = "processing";
    public final static String STATUS_DONE = "done";
    public final static String METHOD = "method";
    public final static String FROM = "from";
    public final static String OK = "ok";
    public final static String INFO = "info";
    public final static String ERROR = "error";
    public final static String USER_ALREADY_EXIST ="userAlreadyExist";
    public final static String SERVER_STATUS = "serverStatus";
    public final static String SERVER = "server";
    public final static String DEVICE = "device";
    public final static String USER = "user";
    public final static String TASK = "task";
    public final static String USER_NOT_AVAILABLE = "userNotAvailable";
    public final static String USER_CREATE = "userCreate";
    public final static String USER_LOGIN_RESPONSE = "userLoginResponse";
    public final static String USER_CREATE_RESPONSE = "userCreateResponse";
    public final static String USER_SHUTDOWN = "userDisconnected";
    public final static String USER_NAME = "username";
    public final static String USER_PASSWORD = "password";
    public final static String USER_LOGIN = "login";
    public final static String USER_UUID = "userUuid";
    public final static String USER_NOT_SECURE = "userNotSecure";
    public final static String DEVICE_NAME = "name";
    public final static String DEVICE_CATEGORY = "category";
    public final static String DEVICE_UUID = "deviceUuid";
    public final static String DEVICE_CREATE = "deviceCreate";
    public final static String DEVICE_CREATE_RESPONSE = "deviceCreateResponse";
    public final static String DEVICE_INFO = "deviceInfo";
    public final static String DEVICE_INFO_RESPONSE = "deviceInfoResponse";
    public final static String DEVICE_LIST = "deviceList";
    public final static String DEVICE_STATUS_CHANGED = "deviceStatusChanged";
    public final static String DEVICE_SHUTDOWN = "deviceDisconnected";
    public final static String DEVICE_SET_TASK = "deviceSetTask";
    public final static String DEVICE_UNIT = "deviceUnit";
    public final static String TASK_CREATE = "taskCreate";
    public final static String TASK_LIST = "taskList";
    public final static String TASK_UNIT = "taskUnit";
    public final static String TASK_CREATE_RESPONSE = "taskCreateResponse";
    public final static String PROPER_DEVICE_NOT_FOUND = "properDeviceNotFound";
    public final static String TASK_ID = "taskId";
    public final static String DEVICE_UPDATE_RESPONSE = "deviceUpdateResponse";

}
