/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.server;

import com.orionasa.factory.protocol.FMProtocol;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.boon.Boon;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author orion
 */
public class DeviceHandler extends Thread {

    protected Socket clientSocket;
    protected Map<String, StoredDevice> deviceTable;
    protected Map<String, String> userTable;
    protected Map<String, Task> taskTable;
    private String uuid;
    protected List activeUserList;
    protected String activeUser;
    protected TaskManager taskManager;

    DeviceHandler(Socket accept, Map<String, StoredDevice> deviceTable, Map<String, String> userTable, Map<String, Task> taskTable, List activeUserList) {
        this.clientSocket = accept;
        this.deviceTable = deviceTable;
        this.userTable = userTable;
        this.taskTable = taskTable;
        this.activeUserList = activeUserList;
        start();
    }

    @Override
    public void run() {

        try {
            System.out.println("New Device Connected");
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
                    true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            Boolean isDisconnected = false;
            String data;
            Map serializedData;
            ObjectMapper objectMapper = new ObjectMapper();
            while ((data = in.readLine()) != null && !isDisconnected) {
                serializedData = objectMapper.readValue(data, Map.class);
                if (serializedData.keySet().contains(FMProtocol.METHOD)) {
                    Map<String, Object> response = new HashMap<>();
                    switch (serializedData.get(FMProtocol.METHOD).toString()) {
                        case FMProtocol.DEVICE_CREATE:
                            // {"from":"device","method":"deviceCreate","name":"Tezgah 1","category":"DOKUM","status":"empty","deviceUnit":"unit"}
                            StoredDevice storedDevice
                                    = new StoredDevice(
                                            serializedData.get(FMProtocol.DEVICE_NAME).toString(),
                                            serializedData.get(FMProtocol.DEVICE_CATEGORY).toString(),
                                            serializedData.get(FMProtocol.STATUS).toString());
                            deviceTable.put(storedDevice.getUuid(), storedDevice);
                            storedDevice.setUnit(serializedData.get(FMProtocol.DEVICE_UNIT).toString());
                            uuid = storedDevice.getUuid();
                            taskManager = new TaskManager(clientSocket, deviceTable, uuid, taskTable);
                            taskManager.startTaskManagement();
                            response.put(FMProtocol.FROM, FMProtocol.SERVER);
                            response.put(FMProtocol.METHOD, FMProtocol.DEVICE_CREATE_RESPONSE);
                            response.put(FMProtocol.STATUS, FMProtocol.OK);
                            response.put(FMProtocol.DEVICE_UUID, uuid);
                            out.println(Boon.toJson(response));
                            break;
                        case FMProtocol.DEVICE_STATUS_CHANGED:
                            // {"from":"device","method":"deviceStatusChanged","status":"empty/busy","taskId":"task"}
                            deviceTable.get(uuid).setStatus(serializedData.get(FMProtocol.STATUS).toString());
                            if (serializedData.keySet().contains(FMProtocol.TASK_ID)) {
                                taskTable.get(serializedData.get(FMProtocol.TASK_ID).toString()).setStatus(FMProtocol.STATUS_DONE);
                                taskManager.setIsProcessed(true);
                            }
                            response.put(FMProtocol.FROM, FMProtocol.SERVER);
                            response.put(FMProtocol.METHOD, FMProtocol.DEVICE_UPDATE_RESPONSE);
                            response.put(FMProtocol.STATUS, FMProtocol.OK);
                            out.println(Boon.toJson(response));
                            break;
                        case FMProtocol.USER_CREATE:
                            // {"from":"user","method":"userCreate","username":"admin","password":"admin","userUuid":"user"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                if (!userTable.containsKey(serializedData.get(FMProtocol.USER_NAME).toString())) {
                                    userTable.put(serializedData.get(FMProtocol.USER_NAME).toString(),
                                            serializedData.get(FMProtocol.USER_PASSWORD).toString());
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.USER_CREATE_RESPONSE);
                                    response.put(FMProtocol.STATUS, FMProtocol.OK);
                                    response.put(FMProtocol.USER, serializedData.get(FMProtocol.USER_NAME).toString());
                                    out.println(Boon.toJson(response));
                                } else {
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.USER_CREATE_RESPONSE);
                                    response.put(FMProtocol.STATUS, FMProtocol.ERROR);
                                    response.put(FMProtocol.INFO, FMProtocol.USER_ALREADY_EXIST);
                                    response.put(FMProtocol.USER, serializedData.get(FMProtocol.USER_NAME).toString());
                                    out.println(Boon.toJson(response));
                                }
                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.USER_LOGIN:
                            // {"from":"user","method":"login","username":"admin","password":"admin"}

                            if (userTable.keySet().contains(serializedData.get(FMProtocol.USER_NAME).toString()) && !activeUserList.contains(serializedData.get(FMProtocol.USER_NAME).toString())
                                    && userTable.get(serializedData.get(FMProtocol.USER_NAME).toString()).equals(serializedData.get(FMProtocol.USER_PASSWORD))) {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_LOGIN_RESPONSE);
                                response.put(FMProtocol.STATUS, FMProtocol.OK);
                                uuid = UUID.randomUUID().toString();
                                activeUserList.add(serializedData.get(FMProtocol.USER_NAME).toString());
                                activeUser = serializedData.get(FMProtocol.USER_NAME).toString();
                                response.put(FMProtocol.USER_UUID, uuid);
                                out.println(Boon.toJson(response));
                            } else {
                                response.put(FMProtocol.METHOD, FMProtocol.USER_LOGIN_RESPONSE);
                                response.put(FMProtocol.STATUS, FMProtocol.ERROR);
                                response.put(FMProtocol.INFO, FMProtocol.USER_NOT_AVAILABLE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.TASK_CREATE:
                            // {"from":"user","method":"taskCreate","userUuid":"user","category":"cat","taskUnit":"14"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                Iterator it = deviceTable.values().iterator();
                                int small = Integer.MAX_VALUE;
                                StoredDevice request = null;
                                while (it.hasNext()) {

                                    StoredDevice device = (StoredDevice) it.next();
                                    if (device.getCategory().equals(serializedData.get(FMProtocol.DEVICE_CATEGORY))) {
                                        if (device.getQue().size() < small) {
                                            small = device.getQue().size();
                                            request = device;
                                        }
                                    }
                                }
                                Task task = new Task(serializedData.get(FMProtocol.DEVICE_CATEGORY).toString(),
                                        serializedData.get(FMProtocol.TASK_UNIT).toString());
                                task.setStatus(FMProtocol.STATUS_QUEUED);
                                if (request != null) {
                                    request.getQue().put(task);
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.TASK_CREATE_RESPONSE);
                                    response.put(FMProtocol.STATUS, FMProtocol.OK);
                                    taskTable.put(task.getId(), task);
                                    out.println(Boon.toJson(response));
                                } else {
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.TASK_CREATE_RESPONSE);
                                    response.put(FMProtocol.STATUS, FMProtocol.ERROR);
                                    response.put(FMProtocol.INFO, FMProtocol.PROPER_DEVICE_NOT_FOUND);
                                    out.println(Boon.toJson(response));
                                }

                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }

                            break;
                        case FMProtocol.TASK_LIST:
                            // {"from":"user","method":"taskList","userUuid":"user"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.TASK_LIST);
                                response.put(FMProtocol.TASK, taskTable);
                                out.println(Boon.toJson(response));
                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.DEVICE_LIST:
                            // {"from":"user","method":"deviceList","userUuid":"user"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                Iterator it = deviceTable.values().iterator();
                                Map<String, Map<String, String>> deviceList = new HashMap<>();
                                while (it.hasNext()) {
                                    Map<String, String> deviceDetailList = new HashMap<>();
                                    StoredDevice device = (StoredDevice) it.next();
                                    deviceDetailList.put("id", device.getUuid());
                                    deviceDetailList.put("name", device.getName());
                                    deviceDetailList.put("category", device.getCategory());
                                    deviceDetailList.put("status", device.getStatus());
                                    deviceDetailList.put("unit",device.getUnit());
                                    deviceList.put(device.getUuid(), deviceDetailList);
                                }
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.DEVICE_LIST);
                                response.put(FMProtocol.DEVICE, deviceList);
                                out.println(Boon.toJson(response));
                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.DEVICE_INFO:
                            // {"from":"user","method":"deviceInfo","userUuid":"user","deviceUuid":"device"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                if (deviceTable.containsKey(serializedData.get(FMProtocol.DEVICE_UUID).toString())) {
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.DEVICE_INFO_RESPONSE);
                                    response.put(FMProtocol.DEVICE_UUID, serializedData.get(FMProtocol.DEVICE_UUID).toString());
                                    response.put(FMProtocol.DEVICE_NAME, deviceTable.get(serializedData.get(FMProtocol.DEVICE_UUID).toString()).getName());
                                    response.put(FMProtocol.DEVICE_CATEGORY, deviceTable.get(serializedData.get(FMProtocol.DEVICE_UUID).toString()).getCategory());
                                    response.put(FMProtocol.STATUS, deviceTable.get(serializedData.get(FMProtocol.DEVICE_UUID).toString()).getStatus());
                                    response.put(FMProtocol.DEVICE_UNIT, deviceTable.get(serializedData.get(FMProtocol.DEVICE_UUID).toString()).getUnit());
                                    Iterator iterator = taskTable.values().iterator();
                                    Map<String, Map<String, String>> taskList = new HashMap<>();
                                    while (iterator.hasNext()) {

                                        Map<String, String> taskDetailList = new HashMap<>();
                                        Task task = (Task) iterator.next();
                                        if (task.getUuid() != null && task.getUuid().equals(serializedData.get(FMProtocol.DEVICE_UUID).toString())) {
                                            taskDetailList.put("id", task.getId());
                                            taskDetailList.put("name", task.getName());
                                            taskDetailList.put("status", task.getStatus());
                                            taskDetailList.put("unit", task.getUnit());
                                            taskList.put(task.getId(), taskDetailList);
                                        }
                                    }
                                    response.put(FMProtocol.TASK, taskList);
                                    out.println(Boon.toJson(response));
                                } else {
                                    response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                    response.put(FMProtocol.METHOD, FMProtocol.DEVICE_INFO_RESPONSE);
                                    response.put(FMProtocol.STATUS, FMProtocol.ERROR);
                                    response.put(FMProtocol.INFO, FMProtocol.PROPER_DEVICE_NOT_FOUND);
                                    out.println(Boon.toJson(response));
                                }

                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.SERVER_STATUS:
                            // {"from":"user","method":"serverStatus","userUuid":"user"}
                            if (serializedData.get(FMProtocol.USER_UUID).equals(uuid)) {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.SERVER_STATUS);
                                response.put(FMProtocol.STATUS, FMProtocol.STATUS_ONLINE);
                                out.println(Boon.toJson(response));
                            } else {
                                response.put(FMProtocol.FROM, FMProtocol.SERVER);
                                response.put(FMProtocol.METHOD, FMProtocol.USER_NOT_SECURE);
                                out.println(Boon.toJson(response));
                            }
                            break;
                        case FMProtocol.DEVICE_SHUTDOWN:
                            // {"from":"device","method":"deviceDisconnected","status":"disconnected"}
                            deviceTable.get(uuid).setStatus(serializedData.get(FMProtocol.STATUS).toString());
                            isDisconnected = true;
                            response.put(FMProtocol.FROM, FMProtocol.SERVER);
                            response.put(FMProtocol.METHOD, FMProtocol.SERVER_STATUS);
                            response.put(FMProtocol.STATUS, FMProtocol.STATUS_DISCONNECTED);
                            out.println(Boon.toJson(response));
                            break;
                        default:
                            System.out.println("Device message " + data);
                            out.println("Unknown message " + data);
                    }
                }

            }
            System.out.println("Device Disconnected " + uuid);
            activeUserList.remove(activeUser);
            deviceTable.remove(uuid);
            out.close();
            in.close();
            clientSocket.close();
        } catch (IOException | InterruptedException e) {
            try {
                System.err.println("Problem with Communication Server " + e);

                clientSocket.close();
            } catch (IOException ex) {
                Logger.getLogger(DeviceHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
