/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author orion
 */
public class ServerUtils extends Thread {

    private final int deviceHandlerPort;

    public ServerUtils(int deviceHandlerPort) {
        this.deviceHandlerPort = deviceHandlerPort;
    }

    public void startServer() {
        start();
    }

    @Override
    public void run() {

        ServerSocket deviceSocket = null;
        try {
            deviceSocket = new ServerSocket(this.deviceHandlerPort);
            System.out.println("Connection Socket Created");
            try {
                Map<String, StoredDevice> deviceTable = Collections.synchronizedMap(new HashMap<String, StoredDevice>());
                Map<String, String> userTable = Collections.synchronizedMap(new HashMap<String, String>());
                Map<String, Task> taskTable = Collections.synchronizedMap(new HashMap<String, Task>());
                List activeUserList = Collections.synchronizedList(new ArrayList<String>());
                Scanner scanner = new Scanner(System.in);
                String username;
                String password;
                System.out.println("Enter admin username ");
                username = scanner.nextLine();
                System.out.println("Enter admin password ");
                password = scanner.nextLine();
                userTable.put(username, password);
                System.out.println("User admin created ");
                System.out.println("Waiting for Connection");
                while (true) {
                    DeviceHandler deviceHandler = new DeviceHandler(deviceSocket.accept(), deviceTable, userTable, taskTable, activeUserList);
                }
            } catch (IOException e) {
                System.err.println("Accept failed.");
                System.exit(1);
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + deviceHandlerPort);
            System.exit(1);
        } finally {
            try {
                deviceSocket.close();
            } catch (IOException e) {
                System.err.println("Could not close port: " + deviceHandlerPort);
                System.exit(1);
            }
        }
    }

}
