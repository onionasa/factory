/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.server;

import java.util.UUID;

/**
 *
 * @author orion
 */
public class Task {

    private final String id;
    private final String name;
    private String uuid;
    private String status;
    private final String unit;

    public Task(String name, String unit) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.unit = unit;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    
    public String getUuid() {
        return uuid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getUnit() {
        return unit;
    }

}
