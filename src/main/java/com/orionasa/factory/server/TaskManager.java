/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.server;

import com.orionasa.factory.protocol.FMProtocol;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.boon.Boon;

/**
 *
 * @author orion
 */
public class TaskManager extends Thread {

    private final Socket sc;
    private final Map<String, StoredDevice> deviceTable;
    private final Map<String, Task> taskTable;
    private final String deviceUuid;
    private final StoredDevice storedDevice;
    private Boolean isProcessed;

    public Boolean getIsProcessed() {
        return isProcessed;
    }

    public synchronized void setIsProcessed(Boolean isProcessed) {
        this.isProcessed = isProcessed;
    }
    
    TaskManager(Socket sc, Map<String, StoredDevice> deviceTable, String deviceUuid,  Map<String, Task> taskTable) {
        this.sc = sc;
        this.deviceTable = deviceTable;
        this.deviceUuid = deviceUuid;
        this.taskTable = taskTable;
        this.storedDevice = this.deviceTable.get(this.deviceUuid);
        this.isProcessed = true;
    }

    public void startTaskManagement() {
        start();
    }

    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(sc.getOutputStream(),
                    true);
            while (true) {
                if(!storedDevice.getQue().isEmpty()){
                    if(storedDevice.getStatus().equals(FMProtocol.STATUS_EMPTY) && isProcessed){
                        Map<String, String> response = new HashMap<>();
                        response.put(FMProtocol.FROM, FMProtocol.SERVER);
                        response.put(FMProtocol.METHOD, FMProtocol.DEVICE_SET_TASK);
                        Task task = (Task) storedDevice.getQue().take();
                        task.setUuid(storedDevice.getUuid());
                        task.setStatus(FMProtocol.STATUS_PROCESSING);
                        response.put(FMProtocol.TASK_UNIT, task.getUnit());
                        response.put(FMProtocol.TASK_ID,task.getId());
                        taskTable.remove(task.getId());
                        taskTable.put(task.getId(), task);
                        isProcessed = false;
                        out.println(Boon.toJson(response));
                    }else if(storedDevice.getStatus().equals(FMProtocol.STATUS_DISCONNECTED)){
                        break;
                    }
                }
            }
        } catch (IOException | InterruptedException e) {
            System.out.println("Task Manager Error "+e);
        }
    }

}
