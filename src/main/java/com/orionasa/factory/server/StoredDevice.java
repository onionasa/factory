/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orionasa.factory.server;

import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author orion
 */
public class StoredDevice {

    private String uuid;
    private String name;
    private String category;
    private String status;
    private String unit;
    private final BlockingQueue que = new LinkedBlockingQueue();
    
    public StoredDevice() {
    }

    public StoredDevice(String name, String category, String status) {
        this.name = name;
        this.category = category;
        this.status = status;
        this.uuid = UUID.randomUUID().toString();
    }

    public BlockingQueue getQue() {
        return que;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    
}
